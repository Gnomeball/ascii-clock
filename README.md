## Simple ASCII Digital clock

Options are as follows:

Flag | Font
----|-----
numbers | Number based font, numbers made out of themselves
big | BIG font, just big, that is all
square | Square-ish font, somewhat square-ish
tiny | tiny font, just tiny.. ish

![Clock](Clock.png)