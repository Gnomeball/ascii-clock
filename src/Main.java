public class Main {

    public static void main(String[] args) {

        Clock clock;

        if (0 < args.length) {
            clock = new Clock(new Font(args[0].substring(1)));
        } else {
            clock = new Clock(new Font());
        }

        clock.run();

    }

}
