import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Clock implements Runnable {

    final Font font;

    public Clock(Font font) {
        this.font = font;
    }

    public void run() {
        try {
            while (true) {
                runClock();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void runClock() throws Exception {

        Process p = Runtime.getRuntime().exec("date -j +%H:%M:%S");
        p.waitFor();
        BufferedReader buf = new BufferedReader(new InputStreamReader(p.getInputStream()));

        displayTime(buf.readLine());
        p.destroy();

        Thread.sleep(1000);
        clearScreen();

    }

    private void displayTime(String time) {
        String hours = time.split(":")[0];
        if (Integer.parseInt(hours) > 12) {
            time = changeToTwelveHour(time);
        } else if (time.charAt(0) == '0') {
            time = time.substring(1);
        }
        printClock(clockArray(time));
    }

    private String changeToTwelveHour(String time) {
        String[] clock = time.split(":");
        String hours = String.valueOf(Integer.parseInt(clock[0]) - 12);
        return hours + ":" + clock[1] + ":" + clock[2];
    }

    private ArrayList<String[]> clockArray(String time) {
        ArrayList<String[]> clock = new ArrayList<>();
        for (char c : time.toCharArray()) {
            clock.add(c == ':' ? font.getDigits()[10] : font.getDigits()[c - '0']);
        }
        return clock;
    }

    private void printClock(ArrayList<String[]> clock) {
        for (int i = 0 ; i < font.getHeight() ; i++) {
            for (String[] strings : clock) {
                System.out.printf("%s", strings[i]);
            }
            System.out.println();
        }
    }

    private void clearScreen() {
        for (int i = 0 ; i < font.getHeight() ; i++) {
            System.out.print("\u001b[1A"); // up a line
            System.out.print("\u001b[2K");
        }
    }

}
